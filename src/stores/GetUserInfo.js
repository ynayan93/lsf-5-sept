import React, { createContext, useCallback, useContext, useEffect, useState } from 'react';
const GetUserInfo = createContext();

//Data manager GetUserInfo
export function UserProvider({ children }) {
    const [user, setUser] = useState(null);
    const userFetch =
    useCallback(() => {
    fetch('/api/current-user/whoami')
    .then(response => response.json())
    .then(data => {
        setUser(data);
        
        console.log("User Data Get");
        console.log(data);
    })
    .catch(error => {
        console.error('Error fetching user information please check api  call in datamanager:', error);
    }
    )
    });

    useEffect(() => {
        userFetch();
      }, [fetch]);
  
    return (
      <GetUserInfo.Provider value={user}>
        {children}
      </GetUserInfo.Provider>
    );
  }
  export function useUser() {
    return useContext(GetUserInfo);
  }
